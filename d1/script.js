console.log('array manipulation')

// Basic Array Structure
// Acces Elements in an array - Through index
// 

// two ways to initialize an array

// let array = [1, 2, 3];
// console.log(array);

// let arr = new Array(1, 2, 3);
// console.log(arr);

// // To Display Index Nubmer
// // Indec = array.length -1
// console.log(array[0]);
// console.log(array[1]);
// console.log(array[2]);


// Array Manipulation

let count = ["one", "two", "three", "four"];

console.log(count.length)
console.log(count[4])

	// using assignment operator (=). to add 
	count[4] = "five"
	console.log(count);

	// Push method. array.push()
		// add element at the end of an array.
	console.log(count.push("element"))
	console.log(count)


	function pushMethod(element){
		return count.push(element)
	}

	pushMethod("six")
	pushMethod("seven")
	pushMethod("eight")

	console.log(count)

// -------------------------------------------

	// Pop Method array.pop()
		// removes the last element of an array.

	count.pop();
	console.log(count);


	// Q: can we remove pop method to remove "four" element?


	function popMethod(element){
		count.pop()
	}
	popMethod();

	console.log(count)

	// Q: how t do we add elemnt at the beggining of an array

	// Unshift method  array.unshift()
		// add an element from the start.
	count.unshift("hugot");
	console.log(count)

	function unshiftMethod(element){
		return count.unshift(element)
	}
	unshiftMethod("zero")
	console.log(count)

// -----------------------------------------
	// to remove an element from the begining

	count.shift()
	console.log(count)





// -----------------------------------------

// Sort Method array.sort()
let nums = [15, 32, 61, 130, 230, 13, 34];

nums.sort();

console.log(nums)


// sort nums in ascending order

nums.sort(
	function(a, b){
		return a - b
	}
)
console.log(nums)



// descending order
nums.sort(
	function(a, b){
		return b - a
	}
)
console.log(nums)



// Reverse Method array.reverse
nums.reverse()

console.log(nums);

// ---------------------------------------------

/* Splice and Slice*/

// Splice Method  array.splice()
	// return an array of omitted element
		
		// first parameter - index where to sstart omitting elemnts

		// second parameter - # of elements to be omitted starting from first parameter.

		// third parameter - elemnts to be added in place of the omitted elemnts


// console.log(count)

// let newSplice = count.splice(1);
// console.log(newSplice);

// let newsplice = count.splice(1, 2);
// console.log(newsplice)
// console.log(count)


// let newSplice = count.splice(1,2, "a1", "b2", "c3")


// Slice Method array.slice()
	
	// Fiirst parameter - ndex where to begin omitting elements

	// seconnd parameter - # of elements to be omitted (index - 1)

console.log(count)

// let newSlice = count.slice(1);
// console.log(newSlice);
// console.log(count)

let newSlice = count.slice(2, 3)
console.log(newSlice)

// ---------------------------------------

// Contact Method array.contact()
	console.log(count)
	console.log(nums)
	let animals = ["bird","cat","dog","fish"];

	let newContact = count.concat(nums, animals);
	console.log(newContact);


// -----------------------------------------


// Jion Method aray.join()

let meal = ["rice", "steak", "juice"]

let newJoin = meal.join();
console.log(newJoin)

 newJoin = meal.join(" ");
console.log(newJoin)

 newJoin = meal.join("-");
console.log(newJoin)

// -------------------------------------------

// toString Method

console.log(nums)
console.log(typeof nums[3]);

let newString = nums.toString()
console.log(typeof newString);


// --------------------------------------------

/*Accessor*/
let countries = ["US","PH","CAN","PH","SG","HK","PH","NZ"];

	// indexOf() array.indexOf()
		// finds the index of a given element where it is "first" found
	let index = countries.indexOf("PH")
	console.log(index);

	// if element is non exsisting, return is -1
	let sample = countries.indexOf("AU")
	console.log(sample);

	let lastIndex = countries.lastIndexOf("PH");
	console.log(lastIndex)


	// if(countries.indexOf("CAN") == -1){
	// 	console.log('Element not existing')
	// } else {
	// 	console.log('element exists in he countries array')
	// }

// --------------------------------------------------

// Iterators

// forEach(call back function())  array.forEach()
// map()	array.map()

let days = ["mon", "tue", "wed", "thu", "fri", "sat","sun"];

// forEach 
	// returns undefined
days.forEach(
	function(element){
		console.log(element)
	}
)

// map 
	// return a copy of an array from the original array which can be manipulated
let mapDays = days.map(function(day){
	return `${day} is the day of the week`
})
console.log(mapDays)
console.log(days)


// MINI ACTIVITY

let days2 = [];

days.forEach(function(day){
	days2.push(day);
})

console.log(days2)





// Filter  array.filter(call backfunction())

console.log(nums)

let newFilter = nums.filter(function(num){
	return num < 50
})

console.log(newFilter)



// includes   array.includes()
	// returns boolean 
console.log(animals)
let newIncludes = animals.includes("dog")
console.log(newIncludes)

// if(countries.indexOf("CAN") == -1){
// 	console.log('Element not existing')
// } else {
// 	console.log('element exists in he countries array')
// }

// let check = '';

// function isFound(val) {
// 	check = animals.includes(val)

// 	if(check === true) {
// 		console.log(val + " is found")
// 	}
// 	else if (check === false) {
// 		console.log(val + "")
// 	}
// }

function miniActivity(name){
	if(animals.includes(name) == true){
		return `${name} is found`
	}else {
		return `${name} is not found`
	}
}

console.log(miniActivity("cat"))
console.log(miniActivity("ex"))
console.log(miniActivity("fish"))


// every 
	//return boolean value.
	// returns true only if "all Elements " passed the given condition

let newEvery = nums.every(function(num){
	return (num > 1)
})
console.log(newEvery)


// some(cb())
	// boolean

	let newSome = nums.every(function(num){
		return (num > 50)
	})
	console.log(newSome);


	// // arrow function
	// let newSome2 = nums.some(num => num > 50);
	// console.log(newSome2);

	// reduced(cb(<previous>, <current>))
	let newReduce = nums.reduce(function(a, b){
		return a + b
	})
	console.log(newReduce)







	/*
	.length - to check how many array
	.push - 
	.pop - to remove the last element
	.unshif - to add to the beggining
	.splice - to remove specific element
	.	-
	.sort - ascending order
	.reverse - like  mirror
	.slice - remove the portioin 


	*/